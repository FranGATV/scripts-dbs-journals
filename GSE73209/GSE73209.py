import pandas as pd
import re
from ydata_profiling import ProfileReport

file_path = '../../Lui/GSE73209/GSE73209_non-normalized.txt'  # Replace with the path to your file

# Read the file and store the lines in a list
with open(file_path, 'r') as file:
    lines = file.readlines()
ids = [x.strip('"') for x in lines[0].split("\t")]
values=[]
# Process the data to create a DataFrame
for line in lines[1:]:
    values.append(line.split("\t"))

# Create a Pandas DataFrame
df = pd.DataFrame(values, columns=ids, )

# Display the resulting DataFrame
print(df.head())
# Creatte the HTML Report 
# Change df.head() for df for a complete Report 
# change the calculate values for diffent correlations on the report 
profile = ProfileReport(df.head(), title="GSE73209 Report",  explorative=True,correlations={
            "auto": {"calculate": False},
            "pearson": {"calculate": False},
            "spearman": {"calculate": False},
            "kendall": {"calculate": False},
            "phi_k": {"calculate": False},
            "cramers": {"calculate": False},
        })
profile.to_file("GSE73209.html")

