import pandas as pd
import re
from ydata_profiling import ProfileReport

file_path = '../../Lee 2014/GSE32664_RAW/GSE32664_complete_RMA_data.txt'  # Replace  with the path to your file

# Read the file and store the lines in a list
with open(file_path, 'r') as file:
    lines = file.readlines()
attribute_names=[]
attribute_values = []
# Process the data to create a DataFrame
for line in lines[2:]:
    attribute_names.append(line.rstrip().split('\t')[0])
    attribute_values.append(line.rstrip().split('\t')[1:])

samples = [x.strip('"') for x in lines[1].rstrip().split('\t')[1:]]

# Create a Pandas DataFrame
df = pd.DataFrame(attribute_values, columns=samples, index=attribute_names)


# Creatte the HTML Report 
# Change df.head() for df for a complete Report 
# change the calculate values for diffent correlations on the report 
profile = ProfileReport(df.head(), title="GSE32664 Report",  explorative=True,correlations={
            "auto": {"calculate": False},
            "pearson": {"calculate": False},
            "spearman": {"calculate": False},
            "kendall": {"calculate": False},
            "phi_k": {"calculate": False},
            "cramers": {"calculate": False},
        })
profile.to_file("GSE32664.html")
