import pandas as pd
import re
from ydata_profiling import ProfileReport

file_path = '../../Lou2020/GSE57370_RAW/GSM1381243_US11153896_253118113873_S01_miRNA_1010_Sep10_2_4.txt'  # Replace 'data.txt' with the path to your file

# Read the file and store the lines in a list
with open(file_path, 'r') as file:
    lines = file.readlines()

info=[]
info.append(lines[1].split("\t")[1:])
info.append(lines[2].split("\t")[1:])

# Create a Pandas DataFrame for Info

df = pd.DataFrame(info).T

# Display the resulting DataFrame
html_table = df.to_html(classes='table table-striped')

# Write the Info HTML table to a file
with open('GSE66405_INFO.html', 'w') as file:
    file.write(f'''
    <!DOCTYPE html>
    <html>
    <head>
        <title>GSE57370 INFO</title>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/css/bootstrap.min.css" rel="stylesheet">
    </head>
    <body>
        <div class="container mt-4">
            <h2>GSE57370 INFO</h2>
            {html_table}
        </div>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/js/bootstrap.bundle.min.js"></script>
    </body>
    </html>
    ''')
# Create a Pandas DataFrame for stats

stats=[]
stats.append(lines[5].split("\t")[1:])
stats.append(lines[6].split("\t")[1:])
df = pd.DataFrame(stats).T

# Display the resulting DataFrame
html_table = df.to_html(classes='table table-striped')

# Write the Stats HTML table to a file
with open('GSE66405_STATS.html', 'w') as file:
    file.write(f'''
    <!DOCTYPE html>
    <html>
    <head>
        <title>GSE66405 STATS</title>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/css/bootstrap.min.css" rel="stylesheet">
    </head>
    <body>
        <div class="container mt-4">
            <h2>GSE66405 STATS</h2>
            {html_table}
        </div>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/js/bootstrap.bundle.min.js"></script>
    </body>
    </html>
    ''')

features=lines[9].split("\t")[1:]
attribute_values=[]
for line in lines[9:100]:
    attribute_values.append(line.split("\t")[1:])

# Create a Pandas DataFrame
df = pd.DataFrame(attribute_values, columns=features)   
 
# Display the resulting DataFrame
print(df.head())

# Creatte the HTML Report 
# Change df.head() for df for a complete Report 
# change the calculate values for diffent correlations on the report 
profile = ProfileReport(df.head(), title="GSE57370 Report",  explorative=True)
profile.to_file("GSE57370.html")
