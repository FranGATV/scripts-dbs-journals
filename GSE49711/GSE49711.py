import pandas as pd
import re
from ydata_profiling import ProfileReport

file_path = '../../Jansky2021/GSE49711/GSE49711_SEQC_NB_MAV_G_log2.20121127.txt'  # Replace  with the path to your file

# Read the file and store the lines in a list
with open(file_path, 'r') as file:
    lines = file.readlines()
attribute_names=[]
attribute_values = []
# Process the data to create a DataFrame
for line in lines[1:]:
    attribute_values.append(line.rstrip().split('\t'))

genes = [x.strip('"') for x in lines[0].rstrip().split('\t')]

# Create a Pandas DataFrame
df = pd.DataFrame(attribute_values, columns=genes,)

# Display the resulting DataFrame
print(df.head())

# Creatte the HTML Report 
# Change df.head() for df for a complete Report 
# change the calculate values for diffent correlations on the report 
profile = ProfileReport(df.head(), title="GSE49711 SEQC NB MAV G",  explorative=True,correlations={
            "auto": {"calculate": False},
            "pearson": {"calculate": False},
            "spearman": {"calculate": False},
            "kendall": {"calculate": False},
            "phi_k": {"calculate": False},
            "cramers": {"calculate": False},
        })
profile.to_file("GSE49711_SEQC_NB_MAV_G.html")


file_path = '../../Jansky2021/GSE49711/GSE49711_SEQC_NB_MAV_J_log2.20121127.txt'  # Replace with the path to your file

# Read the file and store the lines in a list
with open(file_path, 'r') as file:
    lines = file.readlines()
attribute_names=[]
attribute_values = []
# Process the data to create a DataFrame
for line in lines[1:]:
    attribute_values.append(line.rstrip().split('\t'))

genes = [x.strip('"') for x in lines[0].rstrip().split('\t')]

# Create a Pandas DataFrame
df = pd.DataFrame(attribute_values, columns=genes,)

# Display the resulting DataFrame
print(df.head())

# Creatte the HTML Report 
# Change df.head() for df for a complete Report 
# change the calculate values for diffent correlations on the report 
profile = ProfileReport(df.head(), title="GSE49711 SEQC NB MAV J",  explorative=True,correlations={
            "auto": {"calculate": False},
            "pearson": {"calculate": False},
            "spearman": {"calculate": False},
            "kendall": {"calculate": False},
            "phi_k": {"calculate": False},
            "cramers": {"calculate": False},
        })
profile.to_file("GSE49711_SEQC_NB_MAV_J.html")


file_path = '../../Jansky2021/GSE49711/GSE49711_SEQC_NB_MAV_T_log2.20121127.txt'  # Replace with the path to your file

# Read the file and store the lines in a list
with open(file_path, 'r') as file:
    lines = file.readlines()
attribute_names=[]
attribute_values = []
# Process the data to create a DataFrame
for line in lines[1:]:
    attribute_values.append(line.rstrip().split('\t'))

genes = [x.strip('"') for x in lines[0].rstrip().split('\t')]

# Create a Pandas DataFrame
df = pd.DataFrame(attribute_values, columns=genes,)

# Display the resulting DataFrame
print(df.head())

# Creatte the HTML Report 
# Change df.head() for df for a complete Report 
# change the calculate values for diffent correlations on the report 
profile = ProfileReport(df.head(), title="GSE49711 SEQC NB MAV T",  explorative=True,correlations={
            "auto": {"calculate": False},
            "pearson": {"calculate": False},
            "spearman": {"calculate": False},
            "kendall": {"calculate": False},
            "phi_k": {"calculate": False},
            "cramers": {"calculate": False},
        })
profile.to_file("GSE49711_SEQC_NB_MAV_T.html")

file_path = '../../Jansky2021/GSE49711/GSE49711_SEQC_NB_MAV_Th_log2.20121210.txt'  # Replace with the path to your file

# Read the file and store the lines in a list
with open(file_path, 'r') as file:
    lines = file.readlines()
attribute_names=[]
attribute_values = []
# Process the data to create a DataFrame
for line in lines[1:]:
    attribute_values.append(line.rstrip().split('\t'))

genes = [x.strip('"') for x in lines[0].rstrip().split('\t')]

# Create a Pandas DataFrame
df = pd.DataFrame(attribute_values, columns=genes,)

# Display the resulting DataFrame
print(df.head())

# Creatte the HTML Report 
# Change df.head() for df for a complete Report 
# change the calculate values for diffent correlations on the report 
profile = ProfileReport(df.head(), title="GSE49711 SEQC NB MAV Th",  explorative=True,correlations={
            "auto": {"calculate": False},
            "pearson": {"calculate": False},
            "spearman": {"calculate": False},
            "kendall": {"calculate": False},
            "phi_k": {"calculate": False},
            "cramers": {"calculate": False},
        })
profile.to_file("GSE49711_SEQC_NB_MAV_Th.html")


file_path = '../../Jansky2021/GSE49711/GSE49711_SEQC_NB_TAV_G_log2.final.txt'  # Replace  with the path to your file

# Read the file and store the lines in a list
with open(file_path, 'r') as file:
    lines = file.readlines()
attribute_names=[]
attribute_values = []
# Process the data to create a DataFrame
for line in lines[1:]:
    attribute_values.append(line.rstrip().split('\t'))

genes = [x.strip('"') for x in lines[0].rstrip().split('\t')]

# Create a Pandas DataFrame
df = pd.DataFrame(attribute_values, columns=genes,)

# Display the resulting DataFrame
print(df.head())

# Creatte the HTML Report 
# Change df.head() for df for a complete Report 
# change the calculate values for diffent correlations on the report 
profile = ProfileReport(df.head(), title="GSE49711 SEQC NB TAV G",  explorative=True,correlations={
            "auto": {"calculate": False},
            "pearson": {"calculate": False},
            "spearman": {"calculate": False},
            "kendall": {"calculate": False},
            "phi_k": {"calculate": False},
            "cramers": {"calculate": False},
        })
profile.to_file("GSE49711_SEQC_NB_TAV_G.html")

file_path = '../../Jansky2021/GSE49711/GSE49711_SEQC_NB_TAV_J_log2.20121201.txt'  # Replace  with the path to your file

# Read the file and store the lines in a list
with open(file_path, 'r') as file:
    lines = file.readlines()
attribute_names=[]
attribute_values = []
# Process the data to create a DataFrame
for line in lines[1:]:
    attribute_values.append(line.rstrip().split('\t'))

genes = [x.strip('"') for x in lines[0].rstrip().split('\t')]

# Create a Pandas DataFrame
df = pd.DataFrame(attribute_values, columns=genes,)

# Display the resulting DataFrame
print(df.head())

# Creatte the HTML Report 
# Change df.head() for df for a complete Report 
# change the calculate values for diffent correlations on the report 
profile = ProfileReport(df.head(), title="GSE49711 SEQC NB TAV J",  explorative=True,correlations={
            "auto": {"calculate": False},
            "pearson": {"calculate": False},
            "spearman": {"calculate": False},
            "kendall": {"calculate": False},
            "phi_k": {"calculate": False},
            "cramers": {"calculate": False},
        })
profile.to_file("GSE49711_SEQC_NB_TAV_J.html")

file_path = '../../Jansky2021/GSE49711/GSE49711_SEQC_NB_TAV_T_log2.final.txt'  # Replace  with the path to your file

# Read the file and store the lines in a list
with open(file_path, 'r') as file:
    lines = file.readlines()
attribute_names=[]
attribute_values = []
# Process the data to create a DataFrame
for line in lines[1:]:
    attribute_values.append(line.rstrip().split('\t'))

genes = [x.strip('"') for x in lines[0].rstrip().split('\t')]

# Create a Pandas DataFrame
df = pd.DataFrame(attribute_values, columns=genes,)

# Display the resulting DataFrame
print(df.head())

# Creatte the HTML Report 
# Change df.head() for df for a complete Report 
# change the calculate values for diffent correlations on the report 
profile = ProfileReport(df.head(), title="GSE49711 SEQC NB TAV T",  explorative=True,correlations={
            "auto": {"calculate": False},
            "pearson": {"calculate": False},
            "spearman": {"calculate": False},
            "kendall": {"calculate": False},
            "phi_k": {"calculate": False},
            "cramers": {"calculate": False},
        })
profile.to_file("GSE49711_SEQC_NB_TAV_T.html")



file_path = '../../Jansky2021/GSE49711/GSE49711_SEQC_NB_TUC_G_log2.txt'  # Replace  with the path to your file

# Read the file and store the lines in a list
with open(file_path, 'r') as file:
    lines = file.readlines()
attribute_names=[]
attribute_values = []
# Process the data to create a DataFrame
for line in lines[1:]:
    attribute_values.append(line.rstrip().split('\t'))

genes = [x.strip('"') for x in lines[0].rstrip().split('\t')]

# Create a Pandas DataFrame
df = pd.DataFrame(attribute_values, columns=genes,)

# Display the resulting DataFrame
print(df.head())

# Creatte the HTML Report 
# Change df.head() for df for a complete Report 
# change the calculate values for diffent correlations on the report 
profile = ProfileReport(df.head(), title="GSE49711 SEQC NB TUC G",  explorative=True,correlations={
            "auto": {"calculate": False},
            "pearson": {"calculate": False},
            "spearman": {"calculate": False},
            "kendall": {"calculate": False},
            "phi_k": {"calculate": False},
            "cramers": {"calculate": False},
        })
profile.to_file("GSE49711_SEQC_NB_TUC_G.html")

file_path = '../../Jansky2021/GSE49711/GSE49711_SEQC_NB_TUC_J_log2.txt'  # Replace  with the path to your file

# Read the file and store the lines in a list
with open(file_path, 'r') as file:
    lines = file.readlines()
attribute_names=[]
attribute_values = []
# Process the data to create a DataFrame
for line in lines[1:]:
    attribute_values.append(line.rstrip().split('\t'))

genes = [x.strip('"') for x in lines[0].rstrip().split('\t')]

# Create a Pandas DataFrame
df = pd.DataFrame(attribute_values, columns=genes,)

# Display the resulting DataFrame
print(df.head())

# Creatte the HTML Report 
# Change df.head() for df for a complete Report 
# change the calculate values for diffent correlations on the report 
profile = ProfileReport(df.head(), title="GSE49711 SEQC NB TUC J",  explorative=True,correlations={
            "auto": {"calculate": False},
            "pearson": {"calculate": False},
            "spearman": {"calculate": False},
            "kendall": {"calculate": False},
            "phi_k": {"calculate": False},
            "cramers": {"calculate": False},
        })
profile.to_file("GSE49711_SEQC_NB_TUC_J.html")

file_path = '../../Jansky2021/GSE49711/GSE49711_SEQC_NB_TUC_T_log2.txt'  # Replace  with the path to your file

# Read the file and store the lines in a list
with open(file_path, 'r') as file:
    lines = file.readlines()
attribute_names=[]
attribute_values = []
# Process the data to create a DataFrame
for line in lines[1:]:
    attribute_values.append(line.rstrip().split('\t'))

genes = [x.strip('"') for x in lines[0].rstrip().split('\t')]

# Create a Pandas DataFrame
df = pd.DataFrame(attribute_values, columns=genes,)

# Display the resulting DataFrame
print(df.head())

# Creatte the HTML Report 
# Change df.head() for df for a complete Report 
# change the calculate values for diffent correlations on the report 
profile = ProfileReport(df.head(), title="GSE49711 SEQC NB TUC T",  explorative=True,correlations={
            "auto": {"calculate": False},
            "pearson": {"calculate": False},
            "spearman": {"calculate": False},
            "kendall": {"calculate": False},
            "phi_k": {"calculate": False},
            "cramers": {"calculate": False},
        })
profile.to_file("GSE49711_SEQC_NB_TUC_T.html")