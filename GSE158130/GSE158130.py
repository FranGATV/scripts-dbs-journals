import pandas as pd
import re
from ydata_profiling import ProfileReport

file_path = './Jansky2021/GES158130/GSE158130_SK-N-SH_counts.txt'  # Replace with the path to your file

# Read the file and store the lines in a list
with open(file_path, 'r') as file:
    lines = file.readlines()
attribute_names=[]
attribute_values = []
# Process the data to create a DataFrame
for line in lines[1:]:
    match = re.search(r'"([^"]*)"', line)
    attribute_names.append(match.group(1))
    attribute_values.append(line.replace(match.group(0),'').split())

user_ids = [x.strip('"') for x in lines[0].split()]

# Create a Pandas DataFrame
df = pd.DataFrame(attribute_values, columns=user_ids, index=attribute_names).T

# Display the resulting DataFrame
print(df.head())

# Creatte the HTML Report 
# Change df.head() for df for a complete Report 
# change the calculate values for diffent correlations on the report 
profile = ProfileReport(df.head(), title="GSE158130 Report",  explorative=True,correlations={
            "auto": {"calculate": False},
            "pearson": {"calculate": False},
            "spearman": {"calculate": False},
            "kendall": {"calculate": False},
            "phi_k": {"calculate": False},
            "cramers": {"calculate": False},
        })
profile.to_file("GSE158130.html")
