# SCRIPTS FOR DB STUDIED ON THE JOURNAL:
## General context and relevant public datasets available and general for improving pathways in Paediatric Cancer applying Artificial Intelligence. A review.

This is a colleciton of python scripts that creates html reports on public databases that have been studied. The scripts create Pandas Dataframes that can be used to perform further studies on the DBs.

## Use and Installation

Download the DBs

- GSE158130 https://www.ncbi.nlm.nih.gov/geo/query/acc.cgi?acc=GSE158130
- GSE163431  https://www.ncbi.nlm.nih.gov/geo/query/acc.cgi?acc=GSE163431
- GSE32664  https://www.ncbi.nlm.nih.gov/geo/query/acc.cgi?acc=GSE32664
- GSE38419  https://www.ncbi.nlm.nih.gov/geo/query/acc.cgi?acc=GSE38419
- GSE49711  https://www.ncbi.nlm.nih.gov/geo/query/acc.cgi?acc=GSE49711
- GSE163431  https://www.ncbi.nlm.nih.gov/geo/query/acc.cgi?acc=GSE163431
- GSE57370  https://www.ncbi.nlm.nih.gov/geo/query/acc.cgi?acc=GSE57370
- GSE61741  https://www.ncbi.nlm.nih.gov/geo/query/acc.cgi?acc=GSE61741
- GSE64975  https://www.ncbi.nlm.nih.gov/geo/query/acc.cgi?acc=GSE64975
- GSE66405  https://www.ncbi.nlm.nih.gov/geo/query/acc.cgi?acc=GSE66405
- GSE73209  https://www.ncbi.nlm.nih.gov/geo/query/acc.cgi?acc=GSE73209
- GSE74350  https://www.ncbi.nlm.nih.gov/geo/query/acc.cgi?acc=GSE74350


Install requirements

```
pip install -r requirements.txt
```

Replace the scripts path to the tsv, csv, or txt  on the scritps [DBNAME].py with your path

E.g.

```
file_path = '../../Jansky2021/GSE74350/GSE74350_Normalized_data_with_probename.txt'  # 
```

Replace with the path to your file

```
file_path = 'YOUR PATH TO DB/GSE74350_Normalized_data_with_probename.txt'
```


And finally run 

```
python [DBNAME].py
```